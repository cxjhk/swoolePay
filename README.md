# 欢迎使用swoolePay

**swoolePay 基于swoole协程而生 不依赖任何第三方框架 使用一键curl协程即可实现协程化**

## 安装

> composer require kuke/swoole-pay dev-master


## 微信支付
```php
/*
 * 全局配置
 */
include_once dirname(__DIR__) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'mch_id'=>'',
    'key'=>''
]);
```
### 扫码支付
```php
//高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response) use ($tencent){
		$array = $tencent->payment->native->send([
			'body'  =>  '测试购买',
			'spbill_create_ip'  =>  '127.0.0.1',
			'total_fee'  =>  0.01*100,
			'out_trade_no'=>uniqid(),
			'notify_url'=>'http://www.baidu.com'
		]);

		$response->end( "<img src='http://qr.liantu.com/api.php?text={$array['code_url']}'>");		
        });

    $http->start();
```

### jsapi支付 适用于 h5/小程序/公众号
```php
<?php
//高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response) use ($tencent){
		$array = $tencent->payment->jsapi->send([
			'body'  =>  '测试购买',
			'spbill_create_ip'  =>  '127.0.0.1',
			'total_fee'  =>  0.01*100,
			'out_trade_no'=>uniqid(),
			'notify_url'=>'http://www.baidu.com',
			'openid'=>'oVz_s4sSex8DvPRaTyIoQlwVidCE'
		]);

		$response->end(json_encode($array));		
        });

    $http->start();

```
### 公众号授权
```php
<?php
/**
 * 扫码支付
 */
include_once dirname(__DIR__) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'secret'=>''
]);

function getUri()
{
    $uri = $_SERVER['REQUEST_URI'];

    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ?

        "https://": "http://";



    $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    return $url;//输出完整的url
}

$oauth = $tencent->oauth;

$uri = $oauth->setScopes('snsapi_userinfo')->redirect(getUri());


if (empty($_GET['code'])){
    header('location:'.$uri);die;
}

echo '<pre>';

var_dump($oauth->getUserInfo($_GET['code']));
```
#### 未完成功能
	支付退款
	订单查询
	企业付款(暂时没资质)
	后续会将 微信公众号/小程序大部分常用的servier api接入...

### 微信全局配置

| key        | 名称   | 
| --------   | -----:  |
| app_id      | 微信appid   |
| secret     |   微信secret   | 
| mch_id        |    商户mch_id    |
| key        |    商户key    |


### 支付宝全局配置

| key        | 名称   | 
| --------   | -----:  |
| app_id      | 应用id   |
| rsaPrivateKey     |   私钥   | 
| signType        |    RSA2    |

#### 支付宝电脑网站支付


```
    <?php
   
    use KukePay\Ali\Payment\Kernel\ConfigFactory;

    include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
    //高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response){
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            return $response->end();
        }
        $config = ConfigFactory::getInterface();
        $config->setAppId();//支付宝appid
        $config->setRsaPrivateKey();//支付宝私钥
        $ali = \KukePay\PayInterface::ali($config);
        $requestConfigs = array(
            'out_trade_no' => '20200331564515154',
            'total_amount' => 0.01, //单位 元
            'subject' => '测试',  //订单标题
        );
        $response->end($ali->payment->pc->send($requestConfigs));//返回html
    });

    $http->start();
```
 #### 支付宝扫码支付

```
    <?php
   
    use KukePay\Ali\Payment\Kernel\ConfigFactory;

    include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
    //高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response){
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            return $response->end();
        }
        $config = ConfigFactory::getInterface();
        $config->setAppId();//支付宝appid
        $config->setRsaPrivateKey();//支付宝私钥
        $ali = \KukePay\PayInterface::ali($config);
        $requestConfigs = array(
            'out_trade_no' => '20200331564515154',
            'total_amount' => 0.01, //单位 元
            'subject' => '测试',  //订单标题
        );
        $response->end($ali->payment->native->send($requestConfigs));//返回json
    });

    $http->start();
```
#### 支付宝手机网站支付

```
    <?php
  
    use KukePay\Ali\Payment\Kernel\ConfigFactory;

    include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
    //高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response){
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            return $response->end();
        }
        $config = ConfigFactory::getInterface();
        $config->setAppId();//支付宝appid
        $config->setRsaPrivateKey();//支付宝私钥
        $ali = \KukePay\PayInterface::ali($config);
        $requestConfigs = array(
            'out_trade_no' => '20200331564515154',
            'total_amount' => 0.01, //单位 元
            'subject' => '测试',  //订单标题
        );
        $response->end($ali->payment->wap->send($requestConfigs));//返回html
    });

    $http->start();
```
#### 支付宝退款

```
    <?php
  
    use KukePay\Ali\Payment\Kernel\ConfigFactory;

    include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
    //高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response){
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            return $response->end();
        }
        $config = ConfigFactory::getInterface();
        $config->setAppId();//支付宝appid
        $config->setRsaPrivateKey();//支付宝私钥
        $ali = \KukePay\PayInterface::ali($config);
        $requestConfigs = array(
            'out_trade_no' => '20200331564515154',
            'refund_amount' => 0.01, //单位 元
        );
        $response->end($ali->payment->refund->send($requestConfigs));
    });

    $http->start();    
```
#### 支付宝授权
```

    <?php
  
    use KukePay\Ali\Payment\Kernel\ConfigFactory;

    include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
    //高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response){
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            return $response->end();
        }
        $config = ConfigFactory::getInterface();
        $config->setAppId();//支付宝appid
        $config->setRsaPrivateKey();//支付宝私钥

        $ali = \KukePay\PayInterface::ali($config);
        //获取auth_code的方法
        $return_url = '';
        $response->redirect($ali->payment->auth->redirect($return_url));

        //根据auth_code获取token
        $auth_code = $request->get['auth_code'];
        $ali->payment->auth->getAccessToken($auth_code);//返回token

        //根据token获取用户id
        $userInfo = $ali->payment->auth->getUserInfo('token');
            $response->end('');
        });

    $http->start(); 
```
#### 支付宝回调验证签名
```

    <?php
  
    use KukePay\Ali\Payment\Kernel\ConfigFactory;

    include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
    //高性能HTTP服务器
    $http = new Swoole\Http\Server("127.0.0.1", 9501);

    $http->on("request", function (swoole_http_request $request, swoole_http_response $response){
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            return $response->end();
        }
        $config = ConfigFactory::getInterface();
        $config->setAppId();//支付宝appid
        $config->setRsaPrivateKey();//支付宝私钥

        $ali = \KukePay\PayInterface::ali($config);

        $data = $request->post;
        //返回bool值
        $res =  $ali->payment->notify->checkSign($data);

    $http->start(); 
```
## 鸣谢

   [swoole](https://www.swoole.com/)
   
   [yurunhttp](https://github.com/Yurunsoft/YurunHttp)
    
## 注
    swoolePay能为开发者快速完成微信和支付宝支付,统一返回结果让开发者自由处理，我们只负责提交和处理返回