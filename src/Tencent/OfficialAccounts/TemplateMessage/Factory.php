<?php


namespace KukePay\Tencent\OfficialAccounts\TemplateMessage;


use KukePay\Tencent\AccessToken\Application;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;
use KukePay\Tencent\Payment\Kernel\SignFactory;

class Factory extends \KukePay\Kernel\Factory
{
    private $accessToken;
    private $config;
    private $sign;

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
        $this->sign = SignFactory::getInterface();
        if (!isset($this->accessToken)) {
            $this->accessToken = new Application($config);
        }
        parent::__construct();
    }

    /**
     * @param $access_token
     * @param array $form_params
     * @return mixed
     */
    public function send($access_token, array $form_params)
    {
        $uri = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=';
        $response = $this->request->post($uri . $access_token,$form_params,'json')->getBody()->getContents();
        return json_decode($response, true);
    }
}