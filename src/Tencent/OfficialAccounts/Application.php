<?php
namespace KukePay\Tencent\OfficialAccounts;
use KukePay\Tencent\OfficialAccounts\Jssdk\Factory;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;

/**
 * 公众号入口
 * @property Factory $jssdk
 * @property \KukePay\Tencent\OfficialAccounts\TemplateMessage\Factory $templateMessage
 * @package KukePay\Tencent
 */
class Application
{
    /**
     * 全局配置
     * @var array
     */
    private $config;
    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Tencent\\OfficialAccounts\\{$namespace}\\Factory";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}