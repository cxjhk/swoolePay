<?php
namespace KukePay\Tencent;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;

/**
 * 微信统一入口
 * 在此处可以修改统一的request类
 * @property \KukePay\Tencent\Payment\Application $payment
 * @property \KukePay\Tencent\MiniProgram\Application $miniProgram
 * @property \KukePay\Tencent\OfficialAccounts\Application $officialAccounts
 * @property \KukePay\Tencent\AccessToken\Application $accessToken
 * @package KukePay\Tencent
 */
class Application
{
    /**
     * 全局配置
     * @var array
     */
    private $config;
    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Tencent\\{$namespace}\\Application";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}