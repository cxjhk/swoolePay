<?php
namespace KukePay\Tencent\MiniProgram;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;

/**
 * 小程序入口
 * @package KukePay\Tencent
 */
class Application
{
    /**
     * 全局配置
     * @var array
     */
    private $config;
    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Tencent\\Payment\\{$namespace}\\Factory";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}