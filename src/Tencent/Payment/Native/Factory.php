<?php

namespace KukePay\Tencent\Payment\Native;

use KukePay\Tencent\Payment\Kernel\ConfigFactory;
use KukePay\Tencent\Payment\Unifiedorder\Application;

/**
 * 微信扫码支付
 * @package KukePay\Tencent\Native
 */
class Factory
{
    private $config;

    /**
     * Factory constructor.
     * @param array $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * 发起支付
     * @param array $params
     * @return array|string
     */
    public function send(array $params)
    {
        return Application::getInterface($this->config)
            ->send(
                $params,
                'NATIVE',
                function ($response, Application $obj) {
                    $codeUrl = (array)($response->code_url);
                    return array_merge($obj->getParams($response), ['code_url' => $codeUrl[0]]);
                }
            );
    }
}