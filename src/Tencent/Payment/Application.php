<?php
namespace KukePay\Tencent\Payment;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;
use KukePay\Tencent\Payment\Native\Factory;

/**
 * 微信统一入口
 * 在此处可以修改统一的request类
 * @property Factory $native
 * @property \KukePay\Tencent\Payment\Jsapi\Factory $jsapi
 * @property \KukePay\Tencent\Payment\Oauth\Factory $oauth
 * @property \KukePay\Tencent\Payment\Notify\Factory $notify
 * @package KukePay\Tencent
 */
class Application
{
    /**
     * 全局配置
     * @var array
     */
    private $config;
    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Tencent\\Payment\\{$namespace}\\Factory";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}