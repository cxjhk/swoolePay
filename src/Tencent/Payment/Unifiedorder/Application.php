<?php

namespace KukePay\Tencent\Payment\Unifiedorder;

use KukePay\ExceptionHandler;
use KukePay\Kernel\Factory;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;
use KukePay\Tencent\Payment\Kernel\SignFactory;

/**
 * 统一下单接口
 * @package KukePay\Tencent\Unifiedorder
 */
class Application extends Factory
{
    /**
     * 请求url
     * @var string
     */
    private $uri = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
    /**
     * 签名对象
     * @var SignFactory
     */
    private $sign;
    /**
     * @var ConfigFactory
     */
    private $config;

    private static $interface;

    /**
     * @param ConfigFactory $config
     * @return Application
     */
    public static function getInterface(ConfigFactory $config)
    {
        if (!isset(self::$interface)) {
            self::$interface = new self($config);
        }
        return self::$interface;
    }

    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
        $this->sign = SignFactory::getInterface();
        parent::__construct();
    }

    /**
     * 防止元素被克隆
     */
    private function __clone()
    {
    }

    /**
     * @param array $params
     * @param string $trade_type
     * @param callable $function
     * @return string
     */
    public function send(array $params, string $trade_type, callable $function)
    {
        try {
            $unified = [
                'appid' => $this->config->getAppId(),
                'attach' => 'pay',             //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
                'mch_id' => $this->config->getMchId(),
                'nonce_str' => $this->sign->createNonceStr(),
                'trade_type' => $trade_type,
            ];

            foreach ($params as $key => $value) {
                $unified[$key] = $value;
            }

            $unified['notify_url'] = isset($unified['notify_url']) ? $unified['notify_url'] : $this->config->getNotifyUrl();

            $unified['sign'] = $this->sign->getSign($unified, $this->config->getKey());
            //curl
            $response = $this->request->post($this->uri,$this->sign->arrayToXml($unified))->getBody()->getContents();

            libxml_disable_entity_loader(true);
            $unifiedOrder = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);

            if ($unifiedOrder === false) throw new ExceptionHandler('parse xml error');

            if ($unifiedOrder->return_code != 'SUCCESS') throw new ExceptionHandler($unifiedOrder->return_msg);

            if ($unifiedOrder->result_code != 'SUCCESS') throw new ExceptionHandler($unifiedOrder->err_code_des);

            return $function($unifiedOrder, $this);


        } catch (ExceptionHandler $exception) {
            return $exception->getMessage();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * 二次签名获取参数
     * @param $unifiedOrder
     * @return array
     */
    public function getParams($unifiedOrder)
    {
        $arr = [
            "appId" => $this->config->getAppId(),
            "timeStamp" => time(),
            "nonceStr" => $this->sign->createNonceStr(),
            "package" => "prepay_id=" . $unifiedOrder->prepay_id,
            "signType" => 'MD5'
        ];

        $arr['paySign'] = $this->sign->getSign($arr, $this->config->getKey());

        return $arr;
    }
}