<?php

namespace KukePay\Tencent\Payment\Jsapi;

use KukePay\Tencent\Payment\Kernel\ConfigFactory;
use KukePay\Tencent\Payment\Unifiedorder\Application;

/**
 * JSAPI支付
 * @package KukePay\Tencent\Native
 */
class Factory
{
    private $config;

    /**
     * Factory constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * 发起支付
     * @param array $params
     * @return array|string
     */
    public function send(array $params)
    {
        return Application::getInterface($this->config)->send($params,'JSAPI',function ($response,Application $obj){
            return $obj->getParams($response);
        });
    }
}