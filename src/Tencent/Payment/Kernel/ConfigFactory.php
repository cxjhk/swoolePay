<?php


namespace KukePay\Tencent\Payment\Kernel;


class ConfigFactory
{
    private $app_id;
    private $mch_id;
    private $key;
    private $secret;
    private $notify_url;
    private $scene_info;


    /**
     * @return mixed
     */
    public function getSceneInfo()
    {
        return $this->scene_info;
    }

    /**
     * @param mixed $scene_info
     */
    public function setSceneInfo($scene_info)
    {
        $this->scene_info = $scene_info;
    }
    private $cert_path;//绝对路径！！！！
    private $key_path;//绝对路径！！！！
    private static $interface;

    /**
     * @return mixed
     */
    public function getCertPath()
    {
        return $this->cert_path;
    }

    /**
     * @param mixed $cert_path
     */
    public function setCertPath($cert_path)
    {
        $this->cert_path = $cert_path;
    }

    /**
     * @return mixed
     */
    public function getKeyPath()
    {
        return $this->key_path;
    }

    /**
     * @param mixed $key_path
     */
    public function setKeyPath($key_path)
    {
        $this->key_path = $key_path;
    }

    /**
     * @return ConfigFactory
     */
    public static function getInterface()
    {
        if (!isset(self::$interface)) {
            self::$interface = new self();
        }
        return self::$interface;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * @param mixed $app_id
     */
    public function setAppId($app_id)
    {
        $this->app_id = $app_id;
    }

    /**
     * @return mixed
     */
    public function getMchId()
    {
        return $this->mch_id;
    }

    /**
     * @param mixed $mch_id
     */
    public function setMchId($mch_id)
    {
        $this->mch_id = $mch_id;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @return mixed
     */
    public function getNotifyUrl()
    {
        return $this->notify_url;
    }

    /**
     * @param mixed $notify_url
     */
    public function setNotifyUrl($notify_url)
    {
        $this->notify_url = $notify_url;
    }

}