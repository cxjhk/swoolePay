<?php
namespace KukePay;
use KukePay\Tencent\Application;
use KukePay\Tencent\Payment\Kernel\ConfigFactory;

/**
 * @method static Application    tencent(ConfigFactory $config)
 * @method static \KukePay\Ali\Application ali(\KukePay\Ali\Payment\Kernel\ConfigFactory $configFactory)
 * @package KukePay
 */
class PayInterface
{

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    private static function make($name, $arguments)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\{$namespace}\\Application";
        return new $class(...$arguments);
    }

    static function __callStatic($name, $arguments)
    {
        return self::make($name,$arguments);
    }
}