<?php


namespace KukePay\Kernel;


use GuzzleHttp\Client;
use Yurun\Util\HttpRequest;

/**
 * 该类主要实现request请求 用户可以自定义传入
 * @package KukePay\Kernel
 */
class RequestInterface
{
    private $client;
    private static $instance;

    /**
     * @return RequestInterface
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $this->client = new HttpRequest();
    }

    private function __clone(){}

    /**
     * post请求
     * @param null $url
     * @param null $requestBody
     * @param null $contentType
     * @return \Yurun\Util\YurunHttp\Http\Response
     */
    public function post($url = null, $requestBody = null, $contentType = null)
    {
        return  $this->client->post($url,$requestBody,$contentType);
    }

    /**
     * get请求
     * @param $uri
     * @param array $options
     * @return \Yurun\Util\YurunHttp\Http\Response
     */
    public function get($uri,$options = [])
    {
        return $this->client->get($uri,$options);
    }

    public function client()
    {
        return $this->client;
    }

}