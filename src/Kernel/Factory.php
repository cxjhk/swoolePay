<?php


namespace KukePay\Kernel;


use KukePay\Kernel\RequestInterface;

class Factory
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * Factory constructor.
     */
    public function __construct()
    {
        $this->request = RequestInterface::getInstance();
    }

}