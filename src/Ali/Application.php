<?php

namespace KukePay\Ali;

use KukePay\Ali\Payment\Kernel\ConfigFactory;

/**
 * @property \KukePay\Ali\Payment\Application $payment
 * Class Application
 * @package KukePay\Ali
 */
class Application
{
    private $config;

    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Ali\\{$namespace}\\Application";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}