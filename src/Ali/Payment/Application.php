<?php

namespace KukePay\Ali\Payment;

use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Pc\Factory;

/**
 * @property Factory $pc
 * @property \KukePay\Ali\Payment\Native\Factory $native
 * @property \KukePay\Ali\Payment\Wap\Factory $wap
 * @property \KukePay\Ali\Payment\Auth\Factory $auth
 * @property \KukePay\Ali\Payment\Refund\Factory $refund
 * @property \KukePay\Ali\Payment\Jsapi\Factory $jsapi
 * @property \KukePay\Ali\Payment\Notify\Factory $notify
 * @property \KukePay\Ali\Payment\TransferToAliUser\Factory $transferToAliUser
 * Class Application
 * @package KukePay\Ali
 */
class Application
{
    private $config;

    /**
     * Application constructor.
     * @param ConfigFactory $config
     */
    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Ali\\Payment\\{$namespace}\\Factory";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}