<?php


namespace KukePay\Ali\Payment\Wap;


use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Unifiedorder\Application;
use KukePay\ExceptionHandler;

/**
 * 支付宝手机支付
 * Class Factory
 * @package KukePay\Ali\Wap
 */
class Factory
{
    private $config;

    private $method = 'alipay.trade.wap.pay';

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return ConfigFactory|string
     * @throws ExceptionHandler
     */
    public function send(array $params)
    {
        if (!isset($params['product_code'])) $params['product_code'] = 'QUICK_WAP_WAY';
        return Application::getInterface($this->config)->set($params, $this->method)->buildRequestForm();
    }


}