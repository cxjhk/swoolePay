<?php


namespace KukePay\Ali\Payment\Refund;


use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Unifiedorder\Application;
use KukePay\ExceptionHandler;

class Factory
{
    private $config;

    private $method = 'alipay.trade.refund';

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return ConfigFactory|string
     * @throws ExceptionHandler
     */
    public function send(array $params)
    {
        return Application::getInterface($this->config)->set($params, $this->method)->send();
    }


}