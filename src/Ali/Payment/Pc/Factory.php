<?php


namespace KukePay\Ali\Payment\Pc;


use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Unifiedorder\Application;
use KukePay\ExceptionHandler;

/**
 * 支付宝PC支付
 * Class Factory
 * @package KukePay\Ali\Pc
 */
class Factory
{
    private $config;

    private $method = 'alipay.trade.page.pay';

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return ConfigFactory|string
     * @throws ExceptionHandler
     */
    public function send(array $params)
    {
        if (!isset($params['product_code'])) $params['product_code'] = 'FAST_INSTANT_TRADE_PAY';
        return Application::getInterface($this->config)->set($params, $this->method)->buildRequestForm();
    }


}