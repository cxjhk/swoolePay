<?php


namespace KukePay\Ali\Payment\Kernel;


class ConfigFactory
{
    private $uri = 'https://openapi.alipay.com/gateway.do';//沙箱环境请换成沙箱的url

    private $app_id;

    private $rsaPrivateKey;

    private $aliPublicKey;

    private $notify_url = '';

    private $return_url = '';

    private $timeout_express = '2h';

    private $signType = 'RSA2';

    private $scope;

    private $grant_type;

    private $format = 'JSON';

    private $charset = 'utf-8';

    private $version = '1.0';

    private static $interface;

    /**
     * @return mixed
     */
    public function getAliPublicKey()
    {
        return $this->aliPublicKey;
    }

    /**
     * @param mixed $aliPublicKey
     */
    public function setAliPublicKey($aliPublicKey)
    {
        $this->aliPublicKey = $aliPublicKey;
    }


    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getSignType()
    {
        return $this->signType;
    }

    /**
     * @param string $signType
     */
    public function setSignType($signType)
    {
        $this->signType = $signType;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }


    /**
     * @param $config
     * @return ConfigFactory
     */
    public static function getInterface()
    {
        if (!isset(self::$interface)) {
            self::$interface = new self();
        }
        return self::$interface;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * @param mixed $app_id
     */
    public function setAppId($app_id)
    {
        $this->app_id = $app_id;
    }

    /**
     * @return mixed
     */
    public function getRsaPrivateKey()
    {
        return $this->rsaPrivateKey;
    }

    /**
     * @param mixed $rsaPrivateKey
     */
    public function setRsaPrivateKey($rsaPrivateKey)
    {
        $this->rsaPrivateKey = $rsaPrivateKey;
    }

    /**
     * @return string
     */
    public function getNotifyUrl()
    {
        return $this->notify_url;
    }

    /**
     * @param string $notify_url
     */
    public function setNotifyUrl($notify_url)
    {
        $this->notify_url = $notify_url;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->return_url;
    }

    /**
     * @param string $return_url
     */
    public function setReturnUrl($return_url)
    {
        $this->return_url = $return_url;
    }

    /**
     * @return string
     */
    public function getTimeoutExpress()
    {
        return $this->timeout_express;
    }

    /**
     * @param string $timeout_express
     */
    public function setTimeoutExpress($timeout_express)
    {
        $this->timeout_express = $timeout_express;
    }


    /**
     * @return mixed
     */
    public function getGrantType()
    {
        return $this->grant_type;
    }

    /**
     * @param mixed $grant_type
     */
    public function setGrantType($grant_type)
    {
        $this->grant_type = $grant_type;
    }


}