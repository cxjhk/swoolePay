<?php


namespace KukePay\Ali\Payment\TransferToAliUser;


use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Unifiedorder\Application;
use KukePay\ExceptionHandler;

/**
 * 单笔转账到支付宝用户
 * Class Factory
 * @package KukePay\Ali\Payment\TransferToAliUser
 */
class Factory
{
    private $config;

    private $payee_type = 'ALIPAY_LOGONID';

    private $method = 'alipay.fund.trans.toaccount.transfer';

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return ConfigFactory|string
     * @throws ExceptionHandler
     */
    public function send(array $params)
    {
        if (!isset($params['payee_type'])) $params['payee_type'] = $this->payee_type;

        return Application::getInterface($this->config)->set($params, $this->method)->send();
        
    }


}