<?php


namespace KukePay\Ali\Payment\Jsapi;


use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Unifiedorder\Application;

/**
 * jsApi支付
 * Class Factory
 * @package KukePay\Ali\Payment\JsApi
 */
class Factory
{
    private $config;

    private $method = 'alipay.trade.create';

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return string
     * @throws \KukePay\ExceptionHandler
     */
    public function send(array $params)
    {
        return Application::getInterface($this->config)->set($params, $this->method)->send();
    }
}

