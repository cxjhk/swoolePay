<?php


namespace KukePay\Ali\Payment\Native;


use KukePay\Ali\Payment\Kernel\ConfigFactory;
use KukePay\Ali\Payment\Unifiedorder\Application;
use KukePay\ExceptionHandler;

/**
 * 支付宝扫码支付
 * Class Factory
 * @package KukePay\Ali\Native
 */
class Factory
{
    private $config;

    private $method = 'alipay.trade.precreate';

    public function __construct(ConfigFactory $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return ConfigFactory|string
     * @throws ExceptionHandler
     */
    public function send(array $params)
    {
        return Application::getInterface($this->config)->set($params, $this->method)->send();
    }
}